import sqlite3
import a2s
from mcstatus import MinecraftServer

def Server_query():
    
    conn = sqlite3.connect('website-gameserver.sqlite3')
    cur = conn.cursor()
    Servers = list(cur.execute("SELECT * FROM Server")) 
    
    timeout=1.0

    for Server in Servers:
        server_slug=""
        temporaire=""
        attente=0
        server_name=""
    
        #Defini les adresses a utiliser
        if Server[3] is None:
            #if Server[4]=="86.75.200.35":
            #    adresse="192.168.0.3"
            #else:
            adresse = Server[4]
        else:
            adresse = Server[3]
    
    
    
    
    
        #Avec Steam :
        if Server[2]==1:
            try:
                address = (adresse, Server[5])
                Server_infos = a2s.info(address, timeout)
                online=1
            except:
                online=0
                if Server[11]!=0:
                    cur.execute("UPDATE Server SET Player='0' WHERE ID='%s'" %(Server[0]))
    
            if online==1:
                #Nom du jeu sans la version
                for char in Server_infos.server_name:
                    if (char==" " and attente==0):
                        attente=1
                        temporaire=temporaire+char
                    else:
                        if attente>0:
                            if (attente==1 and char=="-"):
                                attente=2
                                temporaire=temporaire+char
                            else:
                                if (attente==2 and char==" "):
                                    attente=3
                                    temporaire=temporaire+char
                                else:
                                    if (attente==3 and char=="("):
                                        break
                                    else:
                                        attente=0
                                        server_name=server_name+temporaire
                                        temporaire = ""
                                        server_name=server_name+char
                        else:
                            server_name=server_name+char
                #Fin du traitement du nom du jeu
    
                #-----Nom valide ?
                if Server[7]!=server_name:
                    cur.execute("UPDATE Server SET Name='%s' WHERE ID='%s'" %(server_name,Server[0]))
    
                #-----Slug valide ?
                for char in server_name:
                    if char==" ":
                        server_slug=server_slug+"-"
                    else:
                        server_slug=server_slug+char
                if Server[8]!=server_slug:
                    cur.execute("UPDATE Server SET Slug='%s' WHERE ID='%s'" %(server_slug,Server[0]))
    
                #-----Player online valide ?
                if Server[11]!=Server_infos.player_count:
                    cur.execute("UPDATE Server SET Player='%s' WHERE ID='%s'" %(Server_infos.player_count,Server[0]))
    
                #Player max valide ?
                if Server[12]!=Server_infos.max_players:
                    cur.execute("UPDATE Server SET Max_Player='%s' WHERE ID='%s'" %(Server_infos.max_players,Server[0]))

            #Online ?
            if Server[10]!=online:
                cur.execute("UPDATE Server SET Running='%s' WHERE ID='%s'" %(online,Server[0]))
                
                
    
    
    
        #Avec Minecraft :
        if Server[2]==2:
            try:
                server = MinecraftServer.lookup("%s:%i" %(adresse, Server[5]))
                Server_infos = server.status().raw
                online=1
            except:
                online=0
                if Server[11]!=0:
                    cur.execute("UPDATE Server SET Player='0' WHERE ID='%s'" %(Server[0]))
    
            if online==1:
                #-----Nom valide ?
                if Server[7]!=Server_infos['description']['text']:
                    cur.execute("UPDATE Server SET Name='%s' WHERE ID='%s'" %(Server_infos['description']['text'],Server[0]))
    
                #-----Slug valide ?
                for char in Server_infos['description']['text']:
                    if char==" ":
                        server_slug=server_slug+"-"
                    else:
                        server_slug=server_slug+char
                if Server[8]!=server_slug:
                    cur.execute("UPDATE Server SET Slug='%s' WHERE ID='%s'" %(server_slug,Server[0]))
    
                #-----Player online valide ?
                if Server[11]!=Server_infos['players']['online']:
                    cur.execute("UPDATE Server SET Player='%s' WHERE ID='%s'" %(Server_infos['players']['online'],Server[0]))

                #-----Player max valide ?
                if Server[12]!=Server_infos['players']['max']:
                    cur.execute("UPDATE Server SET Max_Player='%s' WHERE ID='%s'" %(Server_infos['players']['max'],Server[0]))
            #-----Online ?
            if Server[10]!=online:
                cur.execute("UPDATE Server SET Running='%s' WHERE ID='%s'" %(online,Server[0]))
    
    
    conn.commit()
    conn.close()
    




    #Modification de la table des jeux
    conn = sqlite3.connect('website-gameserver.sqlite3')
    cur = conn.cursor()
    Game_Servers = list(cur.execute("SELECT ID, Running, Player FROM Game_Server"))
    
    for Game in Game_Servers:
        Game_online=0
        Game_player=0
        Servers_for_Game = list(cur.execute("SELECT Running, Player FROM Server WHERE ID_Game='%s'" %(Game[0])))
        if Servers_for_Game:
            for Server_for_Game in Servers_for_Game:
                if Server_for_Game[0]==1:
                    Game_online=1
                    Game_player=Game_player+Server_for_Game[1]
    
        if Game_Servers[1]!=Game_online:
            cur.execute("UPDATE Game_Server SET Running='%s' WHERE ID='%s'" %(Game_online, Game[0]))
        if Game[2]!=Game_player:
            cur.execute("UPDATE Game_Server SET Player='%s' WHERE ID='%s'" %(Game_player, Game[0]))
    

    conn.commit()
    conn.close()