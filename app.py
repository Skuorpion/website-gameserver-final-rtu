from flask import Flask, render_template, request
app = Flask(__name__)

import datetime
import sqlite3

import json

import a2s
from mcstatus import MinecraftServer
import querys

import time
after = int(round(time.time() * 1000))





@app.route('/', methods=['GET', 'POST', ])
@app.route('/index/', methods=['GET', 'POST', ])
def index():
    #conn.close()
    global after
    time_test = int(round(time.time() * 1000))
    if request.method == 'POST':
        if request.form['update']=="update":
            if (after+60000)<time_test:
                after = int(round(time.time() * 1000))
                querys.Server_query()
    if (after+900000)<time_test:
        after = int(round(time.time() * 1000))
        querys.Server_query()

    conn = sqlite3.connect('website-gameserver.sqlite3')
    cur = conn.cursor()
    Game_Server = list(cur.execute("SELECT * FROM Game_Server ORDER BY Name"))
    return render_template('index.html', Game_Server=Game_Server, after=after)
    conn.close()





@app.route('/servers/', methods=['GET', 'POST', ])
@app.route('/servers/<game>/', methods=['GET', 'POST', ])
@app.route('/servers/<game>/<server>/', methods=['GET', 'POST', ])
@app.route('/servers/<game>/<server>/<connection>', methods=['GET', 'POST', ])
def servers(game=None, server=None, connection=None):
    #conn.close()
    global after
    time_test = int(round(time.time() * 1000))
    if request.method == 'POST':
        if request.form['update']=="update":
            if (after+60000)<time_test:
                after = int(round(time.time() * 1000))
                querys.Server_query()
    if (after+900000)<time_test:
        after = int(round(time.time() * 1000))
        querys.Server_query()

    conn = sqlite3.connect('website-gameserver.sqlite3')
    cur = conn.cursor()
    TheGame = list(cur.execute("SELECT * FROM Game_Server WHERE Slug='%s' ORDER BY Name" %game))
    if game:
        if server:
            if connection:
                TheServer = list(cur.execute("SELECT * FROM Server WHERE Slug='%s'" %server))
                return render_template('connection.html', TheGame=TheGame[0], TheServer=TheServer[0], after=after)
            else:
                TheServer = list(cur.execute("SELECT * FROM Server WHERE Slug='%s'" %server))
                return render_template('server.html', TheGame=TheGame[0], TheServer=TheServer[0], after=after)
        else:        
            Servers = list(cur.execute("SELECT * FROM Server WHERE ID_Game='%s' ORDER BY Running DESC, Name ASC" %TheGame[0][0]))
            nb_server=0
            nb_server_online=0
            for server in Servers:
                nb_server=nb_server+1
                if server[10]:
                    nb_server_online=nb_server_online+1
            return render_template('servers.html', TheGame=TheGame[0], Servers=Servers, nb_server=nb_server, nb_server_online=nb_server_online, after=after)
    else:
        Game_Server = list(cur.execute("SELECT * FROM Game_Server ORDER BY Name"))
        return render_template('index.html', Game_Server=Game_Server, after=after)
    conn.close()





@app.route('/minigames/', methods=['GET', 'POST', ])
@app.route('/minigames/<minigame>/', methods=['GET', 'POST', ])
def minigames(minigame=None):
    #conn.close()
    global after
    time_test = int(round(time.time() * 1000))
    if request.method == 'POST':
        if request.form['update']=="update":
            if (after+60000)<time_test:
                after = int(round(time.time() * 1000))
                querys.Server_query()
    if (after+900000)<time_test:
        after = int(round(time.time() * 1000))
        querys.Server_query()

    conn = sqlite3.connect('website-gameserver.sqlite3')
    cur = conn.cursor()
    if minigame:
        TheMinigame = list(cur.execute("SELECT * FROM Minigames_local WHERE Slug='%s'" %minigame))
        return render_template('minigame.html', TheMinigame=TheMinigame[0], after=after)
    else:
        Minigames_global = list(cur.execute("SELECT * FROM Minigames_global ORDER BY Name"))
        Minigames_local = list(cur.execute("SELECT * FROM Minigames_local ORDER BY Name"))
        return render_template('minigames.html', Minigames_global=Minigames_global, Minigames_local=Minigames_local, after=after)
    conn.close()





#@app.errorhandler(500)
@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html')

if __name__ == '__main__':
    # Will make the server available externally as well
    app.run(host='0.0.0.0')
