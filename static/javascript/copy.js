const button = document.querySelector('.js-copytextbtn');

button.addEventListener('click', () => {
    const text = document.querySelector('.js-text');
    const range = document.createRange();

    range.selectNode(text);
    window.getSelection().addRange(range);

    try{
        if(document.execCommand('copy'))
        {
            button.classList.add( 'copied' );

            var temp = setInterval( 
                function()
                {
                    button.classList.remove( 'copied' );
                    clearInterval(temp);
                }
            , 600 );
        }
    }catch(err){
        /*alert('Pas possible de copier.')*/
    }

    window.getSelection().removeAllRanges();
});